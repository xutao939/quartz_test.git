package com.quartz.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Job implements Serializable {

    private int id;
    private String group;
    private String name;
    private String className;
    private String cron;

}
