package com.quartz.quartz1;

import com.quartz.quartzJob.MyJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class Tets1 {

    public static void main(String[] args) throws SchedulerException {

        JobDetail jobDetail = JobBuilder.newJob(MyJob.class)
                .withIdentity("job1","group1")
                .usingJobData("job","jobDetail")//用Data来存值
                .usingJobData("name","jobDetail+Name")//用Data来存值
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger1","trigger1")
                .usingJobData("trigger","jobTrigger")//用Data来存值
                .usingJobData("name","jobTrigger+Name")//用Data来存值
                .startNow()  //设置定时触发或者马上触发
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(1).repeatForever())//设置具体的触发方式,单线程间隔一秒,重复执行
                .build();


        try {

            //创建调度器,这里使用默认的，不需要写配置文件（使用别的方式需要配置文件）
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.scheduleJob(jobDetail,trigger);
            scheduler.start();
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
