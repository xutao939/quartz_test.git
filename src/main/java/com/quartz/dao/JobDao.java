package com.quartz.dao;

import com.quartz.entity.Job;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface JobDao {

    public List<Job> findAllJob();

}
