package com.quartz.controller;

import com.alibaba.fastjson.JSONObject;
import com.quartz.entity.Job;
import com.quartz.service.JobService;
import com.quartz.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Component
public class QuartzJob {

    @Resource
    Scheduler scheduler;

    @Resource
    JobService jobService;

    @Resource
    RedisUtil redisUtil;

    public void start() throws SchedulerException {
        scheduler.start();
    }

    @Scheduled(cron = "0 0/1 * * * ?")
    public void loadJob() throws Exception {

        List<Job> list;
        Object o =  redisUtil.get("jobList");
        if (o == null){
            //数据库遍历数据
            list = jobService.findAllJob();
            System.out.println("从数据库中获取Job数据");
            if (list != null){
                //设置将list以JSON字符串的方式存储
                redisUtil.set("jobList", JSONObject.toJSON(list).toString());
                //设置缓存过期时间为100秒
                redisUtil.expire("jobList",100);
            }else {
                return;
            }
        }else {

            //将JSON字符串转换为List<Job>
            list = JSONObject.parseArray((String) redisUtil.get("jobList"),Job.class);
            System.out.println("从缓存中获取Job数据");
        }


        for (Job job : list){

            //获取一个要修改的触发器的资料，身份，key
            TriggerKey triggerKey = new TriggerKey(job.getName(),job.getGroup());

            //根据key获取要更改的具体的CronTrigger触发器
            CronTrigger cronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);

            //判定，如果没有对应的触发器，就建立任务和触发器
            if (null == cronTrigger){
                createJob(scheduler,job);
            }else {
                //如果已经有对应的触发器，那么就已存在任务，程序转入触发器Cron监测方法
                updateJob(job,job.getCron());
            }
        }
    }

    public void createJob(Scheduler scheduler,Job job) throws SchedulerException {

        Class<org.quartz.Job> clazz;
        try {
            //这就是调度器要执行的类
            clazz = (Class<org.quartz.Job>) Class.forName(job.getClassName());
        } catch (ClassNotFoundException e1) {
            throw new RuntimeException(e1);
        }

        //批量创建任务
        JobDetail jobDetail = JobBuilder.newJob(clazz)
                .withIdentity(job.getName(),job.getGroup())
                .build();
        //获取当前Cron时间
        String cron = job.getCron();
        //创建表达式，构建触发器
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cron).withMisfireHandlingInstructionDoNothing();

        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withSchedule(cronScheduleBuilder)
                .withIdentity(job.getName(),job.getGroup())
                .build();
        scheduler.scheduleJob(jobDetail,cronTrigger);
        log.info("当前job创建成功：{}",job.getName());

    }

    /**
     * 1.此方法会对触发器进行更新，主要更新Cron表达式
     * 2.此方法会判定当前触发器的时间较上一分钟是否存在修改
     * 3.只有判定存在修改时，才会对表达式进行修改
     *
     * @param job   任务实体类
     * @param time  修改后的Cron表达式
     * @throws Exception
     */
    public void updateJob(Job job,String time) throws Exception{

        //创建一个要修改的触发器的资料，身份
        TriggerKey triggerKey = new TriggerKey(job.getName(),job.getGroup());

        //获取要更改的具体的CronTrigger触发器
        CronTrigger cronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
        //获取当前时间
        String oldTime = cronTrigger.getCronExpression();
        if (!oldTime.equals(time)){
            //用修改后的时间更新触发器
            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(time).withMisfireHandlingInstructionDoNothing();
            CronTrigger cronTrigger1 = TriggerBuilder.newTrigger()
                    .withIdentity(job.getName(),job.getGroup())
                    .withSchedule(cronScheduleBuilder)
                    .build();
            scheduler.rescheduleJob(triggerKey,cronTrigger1);
            log.info("监听到修改，任务“{}”发生修改，修改前执行时间为：{} ，修改后执行时间为： {}",job.getName(),oldTime,time);
        }

    }

}
