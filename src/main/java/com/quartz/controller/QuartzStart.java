package com.quartz.controller;

import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class QuartzStart implements ApplicationListener<ApplicationEvent> {

    private static boolean loaded = false;

    @Resource
    QuartzJob quartzJob;

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        if(applicationEvent instanceof ContextRefreshedEvent){
            if(!loaded){//避免多次执行
                loaded = true;
                //定时任务启动
                try {

                    //第一遍加载全部任务
                    quartzJob.loadJob();
                    //全部任务都开始执行
                    quartzJob.start();
                    System.out.println("任务已经启动...啦啦啦啦啦啦啦");
                } catch (SchedulerException se) {
                    se.printStackTrace();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }


        }
    }
}
