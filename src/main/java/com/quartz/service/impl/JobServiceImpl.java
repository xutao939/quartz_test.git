package com.quartz.service.impl;

import com.quartz.dao.JobDao;
import com.quartz.entity.Job;
import com.quartz.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class JobServiceImpl implements JobService {

    @Resource
    JobDao jobDao;

    @Override
    public List<Job> findAllJob() {
        return jobDao.findAllJob();
    }
}
