package com.quartz.service;

import com.quartz.entity.Job;

import java.util.List;

public interface JobService {
    public List<Job> findAllJob();
}
